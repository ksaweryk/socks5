#!/usr/bin/python3

import socket
import struct
from re import compile

global_proxy = ("127.0.0.1", 9050)
IPV4REGEX = compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
CONN_RESP_NAMES = {
    b'\x01': "General failure.",
    b'\x02': "Connection not allowed by ruleset.",
    b'\x03': "Network unreachable.",
    b'\x04': "Host unreachable.",
    b'\x05': "Connection refused by destination host.",
    b'\x06': "TTL expired.",
    b'\x07': "Protocol error.",
    b'\x08': "Address type not supported.",
}


class Socks5Error(Exception):
    """Base exception for socks5 proxy related errors."""

    def __init__(self, msg):
        super().__init__(msg)


class AuthError(Socks5Error):
    """
    Exception for errors regarding proxy authentication.

    Raised when the proxy server doesn't accept the given credentials.
    """

    def __init__(self, msg):
        super().__init__(msg)


class AuthMethodError(Socks5Error):
    """
    Exception for errors regarding proxy authentication methods.

    Raised when no common auth methods with the server are implemented.
    """

    def __init__(self, msg):
        super().__init__(msg)


class VersionByteError(Socks5Error):
    """
    Exception raised when servers responses first byte isn't 0x05.

    If this exception is raised, the server probably isn't a socks5 proxy.
    """

    def __init__(self, msg="Invalid server response. \
Make sure proxy server is a socks5 proxy."):
        super().__init__(msg)


class ConnectError(Socks5Error):
    """Exception for errors regarding opening connection through the proxy."""

    def __init__(self, msg):
        super().__init__(msg)


class Socks5Socket(socket.socket):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._proxy = global_proxy
        self._proxy_auth = None

    def set_proxy(self, addr=global_proxy, auth=None):
        """
        Set information used to connect to a proxy.

        Arguments:
        addr - tuple (host, port); address of proxy.
        auth - tuple (uname, passwd)
            credentials for username/password authentication method.
        """
        self._proxy = addr
        self._proxy_auth = auth

    def connect(self, addr):
        host, port = addr
        if self._proxy:
            super().connect(self._proxy)
            # socks version
            greeting = b"\x05"
            # supported authentication methods
            # if self._proxy_auth: 2 (\x02):
            #   no auth (\x00), uname/passwd auth (\x02)
            # else just no auth
            greeting += b"\x02\x00\x02" if self._proxy_auth else b"\x01\x00"
            self.sendall(greeting)

            ret = self.recv(2)
            if ret[0] != 0x05:
                raise VersionByteError  # first byte must be socks version
            if ret[1] == 0xff:
                # \xff no acceptable auth methods
                raise AuthMethodError("Proxy doesn't support any \
                    implemented auth methods.")
            elif ret[1] == 0x02:  # uname/passwd auth
                auth = b"\x01"  # uname/passwd auth version
                # assuming uname and passwd length <= 255!
                auth += struct.pack("B", len(self._proxy_auth[0]))
                auth += self._proxy_auth[0].encode()
                auth += struct.pack("B", len(self._proxy_auth[1]))
                auth += self._proxy_auth[1].encode()
                self.sendall(auth)
                if self.recv(2) != b"\x05\x00":
                    # \x05 socks version; \x00 success status code
                    raise AuthError("Invalid authentication credentials.")

            # else leaves no auth method

            # \x05 socks version; \x01 - establish a tcp connection;
            # \x00 - reserved, must be null
            request = b"\x05\x01\x00"
            if self.family == socket.AF_INET:  # ipv4
                if IPV4REGEX.match(host):  # it's probably an ipv4 address
                    request += b"\x01"  # \x01 - ipv4 address
                    request += _addr2bytes(host)
                    # match again to only pass the matched part
                else:  # it's propably a domain name to be resolved
                    request += b"\x03"  # \x03 - domain name
                    # assuming host len <= 255
                    request += struct.pack("B", len(host))
                    request += host.encode()
                request += struct.pack("!H", port)
            self.sendall(request)
            ret = self.recv(1024)
            if ret[0] != b"\x05":
                raise VersionByteError  # \x05 socks version
            if ret[1] != b"\x00":
                raise ConnectError("Proxy couldn't establish a connection. {}".format(
                    CONN_RESP_NAMES.get(ret[1], "Invalid status byte ({})".format(hex(ord(ret[1]))))
                ))
            # handshake is over
        else:
            super().connect(addr)


def _addr2bytes(addr):
    """Returns ipv4 address as bytes object."""
    return struct.pack("!BBBB", *[int(octet) for octet in addr.split(".")])
