import setuptools

with open("README.md", "r") as readmef:
    long_desc = readmef.read()

setuptools.setup(
    name="socks5",
    version="0.0.1",
    author="Ksawery Korzeniewski",
    author_email="ksawery@korzeniewski.net",
    description="Wrapper around standard socket to support socks5 proxies.",
    long_description=long_desc,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/example-project",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
