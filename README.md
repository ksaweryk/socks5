# Socks5
Python socket wrapper written from scratch.
Adds socks5 proxy support to the standard library sockets.

Currently only supports tcp connections and ipv4.

### Example usage (similar to standard sockets):

```
from socks5 import Socks5Socket
with Socks5Socket() as sock:
    sock.set_proxy(("127.0.0.1", 9050), ("uname", "passwd"))
    sock.connect(("kaszanka.tk", 80))
    sock.sendall(b"HEAD / HTTP/1.1\r\nHost: kaszanka.tk\r\n\r\n")
    print("Received: " + repr(sock.recv(1024)))
```
